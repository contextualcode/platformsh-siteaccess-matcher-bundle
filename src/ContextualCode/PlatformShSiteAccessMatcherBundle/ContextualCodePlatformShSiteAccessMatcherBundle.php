<?php

namespace ContextualCode\PlatformShSiteAccessMatcherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodePlatformShSiteAccessMatcherBundle extends Bundle
{
    protected $name = 'ContextualCodePlatformShSiteAccessMatcherBundle';
}
