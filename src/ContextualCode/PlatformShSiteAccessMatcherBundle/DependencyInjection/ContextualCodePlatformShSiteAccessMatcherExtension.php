<?php

namespace ContextualCode\PlatformShSiteAccessMatcherBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ContextualCodePlatformShSiteAccessMatcherExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $YamlFileLoader = new Loader\YamlFileLoader(
            $container, new FileLocator(__DIR__ . '/../Resources/config')
        );
        $YamlFileLoader->load('routing.yml');
    }

    public function getAlias()
    {
        return 'contextual_code_platform_sh_site_access_matcher';
    }
}
